﻿using System;
using System.Collections.Generic;

namespace SomeApiProject.Entities
{
	public class Customer : BaseEntity
	{
		public Customer()
		{
			Id = Guid.NewGuid().ToString();
			Accounts = new List<Account>();
		}

		public string LastName { get; set; }

		public string FirstName { get; set; }

		public List<Account> Accounts { get; set; }
	}
}