﻿using System;

namespace SomeApiProject.Entities
{
    public abstract class BaseEntity
    {
        public string Id { get; set; }

        public DateTime Created { get; set; }

        public DateTime Changed { get; set; }
    }
}