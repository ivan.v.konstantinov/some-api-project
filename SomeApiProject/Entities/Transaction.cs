﻿using System;

namespace SomeApiProject.Entities
{
	public class Transaction : BaseEntity
	{
		public Transaction()
		{
			Id = Guid.NewGuid().ToString();
		}

		public string AccountId { get; set; }

		public Account Account { get; set; }

		public decimal Credit { get; set; }
	}
}