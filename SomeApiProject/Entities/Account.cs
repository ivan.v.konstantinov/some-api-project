﻿using System;
using System.Collections.Generic;

namespace SomeApiProject.Entities
{
	public class Account : BaseEntity
	{
		private void Init()
		{
			Id = Guid.NewGuid().ToString();
			Transactions = new List<Transaction>();
		}

		public Account()
		{
			Init();
		}

		public Account(string customerId, decimal initialCredit)
		{
			Init();

			CustomerId = customerId;

			if (initialCredit != 0)
			{
				Transactions.Add(new Transaction { Credit = initialCredit });
			}
		}

		public string CustomerId { get; set; }

		public Customer Customer { get; set; }

		public List<Transaction> Transactions { get; set; }
	}
}