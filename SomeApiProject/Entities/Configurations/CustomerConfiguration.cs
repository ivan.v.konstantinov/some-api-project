﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace SomeApiProject.Entities.Configurations
{
	public class CustomerConfiguration : BaseEntityConfiguration<Customer>
	{
		public override void Configure(EntityTypeBuilder<Customer> builder)
		{
			base.Configure(builder);
			builder
				.ToTable("Customers");
		}
	}
}