﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace SomeApiProject.Entities.Configurations
{
	public abstract class BaseEntityConfiguration<TEntity> : IEntityTypeConfiguration<TEntity>
	where TEntity : BaseEntity
	{
        public virtual void Configure(EntityTypeBuilder<TEntity> builder)
        {
            builder
                .HasKey(p => p.Id);

            builder
                .Property(p => p.Id)
                .HasMaxLength(36);
        }
    }
}