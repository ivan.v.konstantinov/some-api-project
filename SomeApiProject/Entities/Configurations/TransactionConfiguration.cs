﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace SomeApiProject.Entities.Configurations
{
	public class TransactionConfiguration : BaseEntityConfiguration<Transaction>
	{
		public override void Configure(EntityTypeBuilder<Transaction> builder)
		{
			base.Configure(builder);
			builder
				.ToTable("Transactions");

			builder
				.HasOne(p => p.Account)
				.WithMany(p => p.Transactions)
				.HasForeignKey(p => p.AccountId);
		}
	}
}