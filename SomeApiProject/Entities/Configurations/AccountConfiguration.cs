﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace SomeApiProject.Entities.Configurations
{
	public class AccountConfiguration : BaseEntityConfiguration<Account>
	{
		public override void Configure(EntityTypeBuilder<Account> builder)
		{
			base.Configure(builder);
			builder
				.ToTable("Accounts");

			builder
				.HasOne(p => p.Customer)
				.WithMany(p => p.Accounts)
				.HasForeignKey(p => p.CustomerId);
		}
	}
}
