﻿using System.Collections.Generic;

namespace SomeApiProject.Models
{
	public class CustomerViewModel
	{
		public string Id { get; set; }

		public string LastName { get; set; }

		public string FirstName { get; set; }

		public decimal Balance { get; set; }

		public List<TransactionViewModel> Transactions { get; set; }
	}
}