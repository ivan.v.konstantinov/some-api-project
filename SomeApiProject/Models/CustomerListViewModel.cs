﻿using System.Collections.Generic;

namespace SomeApiProject.Models
{
	public class CustomerListViewModel
	{
		public string Id { get; set; }

		public string LastName { get; set; }

		public string FirstName { get; set; }
	}
}