﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Threading.Tasks;

namespace SomeApiProject.Models
{
	public class AccountViewModel
	{
		public string Id { get; set; }

		public DateTime Created { get; set; }

		public decimal Balance { get; set; }

		public List<TransactionViewModel> Transactions { get; set; }
	}
}
