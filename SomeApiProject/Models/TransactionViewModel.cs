﻿using System;

namespace SomeApiProject.Models
{
	public class TransactionViewModel
	{
		public decimal Credit { get; set; }

		public DateTime Created { get; set; }

		public string AccountId { get; set; }
	}
}