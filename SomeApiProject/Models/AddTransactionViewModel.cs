﻿namespace SomeApiProject.Models
{
	public class AddTransactionViewModel
	{
		public string AccountId { get; set; }

		public decimal Credit { get; set; }
	}
}