﻿namespace SomeApiProject.Models
{
	public class AddAccountViewModel
	{
		public string CustomerId { get; set; }

		public decimal InitialCredit { get; set; }
	}
}