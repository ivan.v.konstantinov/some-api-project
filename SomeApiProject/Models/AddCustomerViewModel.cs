﻿namespace SomeApiProject.Models
{
	public class AddCustomerViewModel
	{
		public string LastName { get; set; }

		public string FirstName { get; set; }
	}
}