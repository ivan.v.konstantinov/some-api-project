﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using SomeApiProject.Data;
using SomeApiProject.Entities;
using SomeApiProject.Models;

namespace SomeApiProject.Controllers
{
	[Route("[controller]")]
	[ApiController]
	public class CustomerController : ControllerBase
	{
		private readonly SomeApiDbContext _dbContext;

		public CustomerController(SomeApiDbContext dbContext)
		{
			_dbContext = dbContext;
		}

		[HttpGet("list")]
		[Produces("application/json")]
		public ActionResult<List<CustomerListViewModel>> GetList()
		{
			var customers = _dbContext.Customers
				.OrderBy(c => c.LastName)
				.Select(c => new CustomerListViewModel
				{
					FirstName = c.FirstName,
					LastName = c.LastName,
					Id = c.Id
				})
				.ToList();

			return Ok(customers);
		}

		/// <summary>
		/// API from Task Requirements
		/// - Another Endpoint will output the user information showing Name, Surname, balance, and transactions of the accounts.
		/// </summary>
		[HttpGet("{customerId}")]
		[Produces("application/json")]
		public ActionResult<CustomerViewModel> GetById([FromRoute] string customerId)
		{
			if (string.IsNullOrEmpty(customerId))
			{
				return BadRequest($"Specify {nameof(customerId)} in route");
			}

			var customer = _dbContext.Customers.FirstOrDefault(c => c.Id == customerId);

			if (customer == null)
			{
				return BadRequest($"There is no {nameof(Customer)} with id {customerId}");
			}

			var transactions = _dbContext
				.Transactions
				.Where(t => t.Account.CustomerId == customer.Id)
				.OrderByDescending(t => t.Created)
				.Select(t => new TransactionViewModel
				{ 
					AccountId = t.AccountId,
					Created = t.Created,
					Credit = t.Credit
				})
				.ToList();

			var balance = transactions.Sum(t => t.Credit);
			
			var result = new CustomerViewModel
			{
				Id = customer.Id,
				FirstName = customer.FirstName,
				LastName = customer.LastName,
				Balance = balance,
				Transactions = transactions
			};

			return Ok(result);
		}

		[HttpPost("add")]
		[Consumes("application/json")]
		public ActionResult<string> Add([FromBody] AddCustomerViewModel request)
		{
			if (request == null)
			{
				return BadRequest("Specify request body");
			}

			if (string.IsNullOrEmpty(request.LastName) || string.IsNullOrEmpty(request.FirstName))
			{
				return BadRequest($"Set {nameof(request.LastName)} or {nameof(request.FirstName)}");
			}

			var customer = new Customer
			{
				FirstName = request.FirstName,
				LastName = request.LastName
			};

			_dbContext.Customers.Add(customer);
			_dbContext.SaveChanges();

			return Ok(customer.Id);
		}
	}
}