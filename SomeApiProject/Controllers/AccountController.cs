﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SomeApiProject.Data;
using SomeApiProject.Entities;
using SomeApiProject.Models;

namespace SomeApiProject.Controllers
{
	[Route("[controller]")]
	[ApiController]
	public class AccountController : ControllerBase
	{
		private readonly SomeApiDbContext _dbContext;

		public AccountController(SomeApiDbContext dbContext)
		{
			_dbContext = dbContext;
		}

		/// <summary>
		/// API from Task Requirements
		/// - The API will expose an endpoint which accepts the user information (customerID, initialCredit).
		/// - Once the endpoint is called, a new account will be opened connected to the user whose ID is customerID.
		/// - Also, if initialCredit is not 0, a transaction will be sent to the new account.
		/// </summary>
		[HttpPost("open-new")]
		[Consumes("application/json")]
		public ActionResult<string> OpenNew([FromBody] AddAccountViewModel request)
		{
			if (request == null)
			{
				return BadRequest("Specify request body as a json-object");
			}

			if (string.IsNullOrEmpty(request.CustomerId))
			{
				return BadRequest($"Specify {nameof(request.CustomerId)}");
			}

			var customer = _dbContext.Customers.FirstOrDefault(c => c.Id == request.CustomerId);

			if (customer == null)
			{
				return BadRequest($"There is no {nameof(Customer)} with id {request.CustomerId}");
			}

			var account = new Account(request.CustomerId, request.InitialCredit);

			customer.Accounts.Add(account);
			_dbContext.Update(customer);
			_dbContext.SaveChanges();

			return Ok(account.Id);
		}

		[HttpGet("list-by-customerId/{customerId}")]
		[Produces("application/json")]
		public ActionResult<List<AccountViewModel>> GetListByCustomerId([FromRoute] string customerId)
		{
			if (string.IsNullOrEmpty(customerId))
			{
				return BadRequest($"Specify {nameof(customerId)} in route");
			}

			var isExists = _dbContext.Customers.Any(c => c.Id == customerId);
			if (!isExists)
			{
				return BadRequest($"There is no {nameof(Customer)} with id {customerId}");
			}

			var accounts = _dbContext
				.Accounts
				.Where(a => a.CustomerId == customerId)
				.Select(a => new AccountViewModel
				{
					Id = a.Id,
					Created = a.Created,
					Balance = a.Transactions.Sum(t => t.Credit)
				})
				.ToList();

			return Ok(accounts);
		}

		[HttpGet("{accountId}")]
		public ActionResult<AccountViewModel> GetById([FromRoute] string accountId)
		{
			if (string.IsNullOrEmpty(accountId))
			{
				return BadRequest($"Specify {nameof(accountId)} in route");
			}

			var account = _dbContext
				.Accounts
					.Include(a => a.Transactions)
				.FirstOrDefault(a => a.Id == accountId);

			if (account == null)
			{
				return BadRequest($"There is no {nameof(Account)} with id {accountId}");
			}

			var transactions = account
				.Transactions
				.Select(t => new TransactionViewModel 
				{ 
					AccountId = account.Id, 
					Created = t.Created, 
					Credit = t.Credit 
				})
				.ToList();

			var result = new AccountViewModel
			{
				Id = account.Id,
				Balance = transactions.Sum(t => t.Credit),
				Created = account.Created,
				Transactions = transactions
			};

			return Ok(result);
		}
	}
}