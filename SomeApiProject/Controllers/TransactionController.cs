﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using SomeApiProject.Data;
using SomeApiProject.Entities;
using SomeApiProject.Models;

namespace SomeApiProject.Controllers
{
	[Route("[controller]")]
	[ApiController]
	public class TransactionController : ControllerBase
	{
		private readonly SomeApiDbContext _dbContext;

		public TransactionController(SomeApiDbContext dbContext)
		{
			_dbContext = dbContext;
		}

		[HttpPost("add")]
		[Consumes("application/json")]
		public ActionResult Add([FromBody] AddTransactionViewModel request)
		{
			if (request == null)
			{
				return BadRequest("Specify request body");
			}

			if (string.IsNullOrEmpty(request.AccountId))
			{
				return BadRequest($"Set {nameof(request.AccountId)}");
			}

			if (request.Credit == 0)
			{
				return BadRequest("Can not add transaction with zero credit");
			}

			var account = _dbContext.Accounts.FirstOrDefault(a => a.Id == request.AccountId);

			if (account == null)
			{
				return BadRequest($"There is no {nameof(Account)} with id {request.AccountId}");
			}

			account.Transactions.Add(new Transaction { Credit = request.Credit });
			_dbContext.Update(account);
			_dbContext.SaveChanges();

			return Ok();
		}


		[HttpGet("list-by-account/{accountId}")]
		[Produces("application/json")]
		public ActionResult<List<TransactionViewModel>> GetListByAccountId([FromRoute] string accountId)
		{
			if (string.IsNullOrEmpty(accountId))
			{
				return BadRequest($"Specify {nameof(accountId)} in route");
			}

			var isExists = _dbContext.Accounts.Any(a => a.Id == accountId);
			if (!isExists)
			{
				return BadRequest($"There is no {nameof(Account)} with id {accountId}");
			}

			var transactions = _dbContext
				.Transactions
				.Where(t => t.Account.Id == accountId)
				.OrderByDescending(t => t.Created)
				.Select(t => new TransactionViewModel
				{
					Credit = t.Credit,
					Created = t.Created,
					AccountId = t.AccountId
				})
				.ToList();

			return Ok(transactions);
		}
	}
}