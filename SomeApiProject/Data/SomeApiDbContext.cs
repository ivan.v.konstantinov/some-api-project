﻿using Microsoft.EntityFrameworkCore;
using SomeApiProject.Entities;
using SomeApiProject.Entities.Configurations;
using System;
using System.Linq;

namespace SomeApiProject.Data
{
	public class SomeApiDbContext : DbContext
	{
		public SomeApiDbContext(DbContextOptions<SomeApiDbContext> options) : base(options)
		{
		}

		public DbSet<Account> Accounts { get; set; }

		public DbSet<Customer> Customers { get; set; }

		public DbSet<Transaction> Transactions { get; set; }

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			base.OnModelCreating(modelBuilder);
			modelBuilder.ApplyConfiguration(new CustomerConfiguration());
			modelBuilder.ApplyConfiguration(new AccountConfiguration());
			modelBuilder.ApplyConfiguration(new TransactionConfiguration());
		}

		public override int SaveChanges()
		{
			var entries = ChangeTracker
				.Entries()
				.Where(e => e.Entity is BaseEntity && (e.State == EntityState.Added || e.State == EntityState.Modified));

			foreach (var entityEntry in entries)
			{
				((BaseEntity)entityEntry.Entity).Changed = DateTime.Now;

				if (entityEntry.State == EntityState.Added)
				{
					((BaseEntity)entityEntry.Entity).Created = DateTime.Now;
				}
			}

			return base.SaveChanges();
		}
	}
}