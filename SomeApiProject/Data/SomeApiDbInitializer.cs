﻿using SomeApiProject.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SomeApiProject.Data
{
	public static class SomeApiDbInitializer
	{
		public const string Customer1Id = "5b3ecef1-a9cf-4ae2-9382-37caf1744276";
		public const string Account1_1Id = "571f96ca-9bb0-483a-999e-10db161d34dd";

		public static void Initialize(this SomeApiDbContext context)
		{
			context.Database.EnsureCreated();

			if (context.Customers.Any())
			{
				return;
			}

			var customer1 = new Customer
			{ 
				Id = Customer1Id, 
				FirstName = "John", 
				LastName = "Konstantin", 
				Created = new DateTime(2020, 1, 12, 11, 30, 00),
				Changed = new DateTime(2020, 2, 23, 18, 08, 00)
			};

			var account1_1 = new Account
			{
				Id = Account1_1Id,
				Created = new DateTime(2020, 2, 23, 18, 08, 00),
				Changed = new DateTime(2020, 2, 23, 18, 28, 00),
				Transactions = new List<Transaction>
				{ 
					new Transaction
					{
						Id = "3f0406cc-2159-4b90-9794-27cc3b89c585",
						Created = new DateTime(2020, 2, 23, 18, 28, 00),
						Credit = 101
					},
					new Transaction
					{
						Id = "3a295778-df6f-42ef-864b-3030448b5d6b",
						Created = new DateTime(2020, 2, 23, 19, 28, 00),
						Credit = -21
					}
				}
			};

			customer1.Accounts.Add(account1_1);

			var customers = new Customer[]
			{
				customer1,
				new Customer{ Id = "2f186cc2-8a3a-4b22-a531-c4d2ca4dc8d8", FirstName = "Meredith", LastName = "Alonso", Created = DateTime.Parse("2002-09-01") },
				new Customer{ Id = "73375037-cdd2-4adb-ab8d-7b42bff45a7d", FirstName = "Arturo", LastName = "Anand", Created = DateTime.Parse("2003-09-01")}
			};
			
			context.Customers.AddRange(customers);

			context.SaveChanges();
		}
	}
}
