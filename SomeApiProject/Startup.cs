using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using SomeApiProject.Data;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using System.Net;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.OpenApi.Models;
using System.Reflection;
using System.IO;

namespace SomeApiProject
{
	public class Startup
	{
		private const string _swaggerApiTitle = "Some API";

		public void ConfigureServices(IServiceCollection services)
		{
			services.AddSwaggerGen(options =>
			{
				options.SwaggerDoc("v1", new OpenApiInfo
				{
					Title = _swaggerApiTitle,
					Version = "v1",
					Description = "Some API interactive documentation here",
				});

				var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
				var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
				options.IncludeXmlComments(xmlPath);
			});

			services.AddDbContext<SomeApiDbContext>(options => options.UseInMemoryDatabase("SomeApiDb"));

			services.AddControllers().AddApplicationPart(typeof(Startup).Assembly);
		}

		public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
		{
			app.UseExceptionHandler(errorApp =>
				errorApp.Run(async context =>
				{
					var exceptionHandler = context.Features.Get<IExceptionHandlerFeature>();

					Log.Error(exceptionHandler.Error, "The environment have cought an exception");

					context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
					context.Response.ContentType = "application/json";
					await context.Response.WriteAsync($"{{ \"exception\": \"{exceptionHandler.Error.Message}\" }}");
				}));

			app.UseSwagger();

			app.UseSwaggerUI(c =>
			{
				c.SwaggerEndpoint("../swagger/v1/swagger.json", _swaggerApiTitle);
			});

			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}

			app.UseRouting();

			app.UseCors(builder =>
			{
				builder
					.AllowAnyOrigin()
					.AllowAnyHeader()
					.AllowAnyMethod();
			});

			app.UseEndpoints(endpoints =>
			{
				endpoints.MapControllers();
			});
		}
	}
}