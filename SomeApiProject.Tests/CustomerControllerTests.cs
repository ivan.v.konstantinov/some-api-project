using Microsoft.AspNetCore.Mvc;
using SomeApiProject.Controllers;
using SomeApiProject.Data;
using SomeApiProject.Models;
using SomeApiProject.Tests.Infrastructure;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace SomeApiProject.Tests
{
	public class CustomerControllerTests : BaseTestController
	{
		private CustomerController _customerController { get; set; }

		public CustomerControllerTests(ApplicationContextsDataFixture fixture) : base(fixture)
		{
			_customerController = new CustomerController(_fixture.SomeApiContext);
		}

		[Fact]
		public void GetById_IfRequestEmpty_Fail()
		{
			var result = _customerController.GetById(null);
			Assert.IsType<ActionResult<CustomerViewModel>>(result);
			Assert.True(result.Result is BadRequestObjectResult);
		}

		[Fact]
		public void GetById_IfCutomerNotExists_Fail()
		{
			var result = _customerController.GetById(BadIdentifier);
			Assert.IsType<ActionResult<CustomerViewModel>>(result);
			Assert.True(result.Result is BadRequestObjectResult);
		}

		[Fact]
		public void GetById_IfCutomerExists_Success()
		{
			var result = _customerController.GetById(SomeApiDbInitializer.Customer1Id);
			Assert.IsType<ActionResult<CustomerViewModel>>(result);
			var customerViewModel = ((CustomerViewModel)((OkObjectResult)result.Result).Value);
			Assert.True(customerViewModel != null);
		}

		[Fact]
		public void GetList_IfCutomerExists_Success()
		{
			var result = _customerController.GetList();
			Assert.IsType<ActionResult<List<CustomerListViewModel>>>(result);
			var customers = ((List<CustomerListViewModel>)((OkObjectResult)result.Result).Value);
			Assert.True(customers != null);
			Assert.True(customers.Count == 3);
		}

		[Fact]
		public void Add_IfRequestEmpty_Fail()
		{
			var result = _customerController.Add(null);
			Assert.IsType<ActionResult<string>>(result);
			Assert.True(result.Result is BadRequestObjectResult);
		}

		[Fact]
		public void Add_IfRequestFieldsEmpty_Fail()
		{
			var result = _customerController.Add(new AddCustomerViewModel());
			Assert.IsType<ActionResult<string>>(result);
			Assert.True(result.Result is BadRequestObjectResult);
		}

		[Fact]
		public void Add_IfRequestOneOfFieldsEmpty_Fail()
		{
			var result = _customerController.Add(new AddCustomerViewModel { FirstName = "a"});
			Assert.IsType<ActionResult<string>>(result);
			Assert.True(result.Result is BadRequestObjectResult);
			result = _customerController.Add(new AddCustomerViewModel { LastName = "a" });
			Assert.IsType<ActionResult<string>>(result);
			Assert.True(result.Result is BadRequestObjectResult);
		}

		[Fact]
		public void Add_IfRequestCorrect_Success()
		{
			var result = _customerController
				.Add(
					new AddCustomerViewModel 
					{ 
						FirstName = "Andy", 
						LastName = "Samberg" 
					});
			
			Assert.IsType<ActionResult<string>>(result);

			var customerId = ((string)((OkObjectResult)result.Result).Value);
			Assert.True(!string.IsNullOrEmpty(customerId));

			var customer = _fixture.SomeApiContext.Customers.FirstOrDefault(c => c.Id == customerId);
			Assert.True(customer != null);
		}
	}
}