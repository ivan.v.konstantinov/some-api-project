﻿using Microsoft.AspNetCore.Mvc;
using SomeApiProject.Controllers;
using SomeApiProject.Data;
using SomeApiProject.Models;
using SomeApiProject.Tests.Infrastructure;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace SomeApiProject.Tests
{
	public class TransactionControllerTests : BaseTestController
	{
		private TransactionController _transactionController { get; set; }

		public TransactionControllerTests(ApplicationContextsDataFixture fixture) : base(fixture)
		{
			_transactionController = new TransactionController(_fixture.SomeApiContext);
		}

		[Fact]
		public void Add_IfRequestEmpty_Fail()
		{
			var result = _transactionController.Add(null);
			Assert.True(result is BadRequestObjectResult);
		}

		[Fact]
		public void Add_IfAccountIdEmpty_Fail()
		{
			var result = _transactionController.Add(new AddTransactionViewModel());
			Assert.True(result is BadRequestObjectResult);
		}

		[Fact]
		public void Add_IfCreditIsZero_Fail()
		{
			var result = _transactionController.Add(new AddTransactionViewModel { AccountId = SomeApiDbInitializer.Account1_1Id});
			Assert.True(result is BadRequestObjectResult);
		}

		[Fact]
		public void Add_IfAccountNotExists_Fail()
		{
			var result = _transactionController.Add(new AddTransactionViewModel { AccountId = BadIdentifier });
			Assert.True(result is BadRequestObjectResult);
		}

		[Fact]
		public void Add_IfRequestCorrect_Success()
		{
			var balance = _fixture
				.SomeApiContext
				.Transactions
				.Where(t => t.AccountId == SomeApiDbInitializer.Account1_1Id)
				.Sum(t => t.Credit);

			decimal credit = 100;
			var result = _transactionController
				.Add(
				new AddTransactionViewModel 
				{ 
					AccountId = SomeApiDbInitializer.Account1_1Id,
					Credit = credit
				});

			Assert.True(result is OkResult);

			var newBalance = _fixture
				.SomeApiContext
				.Transactions
				.Where(t => t.AccountId == SomeApiDbInitializer.Account1_1Id)
				.Sum(t => t.Credit);

			Assert.True(balance + credit == newBalance);
		}

		[Fact]
		public void GetListByAccountId_IfRequestEmpty_Fail()
		{
			var result = _transactionController.GetListByAccountId(null);
			Assert.IsType<ActionResult<List<TransactionViewModel>>>(result);
			Assert.True(result.Result is BadRequestObjectResult);
		}

		[Fact]
		public void GetListByAccountId_IfAccountNotExists_Fail()
		{
			var result = _transactionController.GetListByAccountId(BadIdentifier);
			Assert.IsType<ActionResult<List<TransactionViewModel>>>(result);
			Assert.True(result.Result is BadRequestObjectResult);
		}

		[Fact]
		public void GetListByAccountId_IfRequestCorrect_Success()
		{
			var result = _transactionController.GetListByAccountId(SomeApiDbInitializer.Account1_1Id);
			Assert.IsType<ActionResult<List<TransactionViewModel>>>(result);
			var count = ((List<TransactionViewModel>)((OkObjectResult)result.Result).Value).Count;
			Assert.True(count > 0);
		}
	}
}
