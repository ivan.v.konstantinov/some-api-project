﻿using System;
using Microsoft.EntityFrameworkCore;
using SomeApiProject.Data;
using Microsoft.EntityFrameworkCore.Diagnostics;

namespace SomeApiProject.Tests.Infrastructure
{
	public class ApplicationContextsDataFixture : IDisposable
	{
		public SomeApiDbContext SomeApiContext { get; private set; }

		public ApplicationContextsDataFixture()
		{
			var adminApiOptions = new DbContextOptionsBuilder<SomeApiDbContext>()
				.UseInMemoryDatabase(databaseName: $"SomeApiTestInMemoryDb-{Guid.NewGuid()}")
				.ConfigureWarnings(x => x.Ignore(InMemoryEventId.TransactionIgnoredWarning))
				.Options;

			SomeApiContext = new SomeApiDbContext(adminApiOptions);
			SomeApiContext.Initialize();
			SomeApiContext.SaveChanges();
		}

		public void Dispose()
		{
			SomeApiContext.Dispose();
		}
	}
}