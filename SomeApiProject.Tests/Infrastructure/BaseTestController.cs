﻿using Xunit;

namespace SomeApiProject.Tests.Infrastructure
{
	public abstract class BaseTestController : IClassFixture<ApplicationContextsDataFixture>
	{
		protected ApplicationContextsDataFixture _fixture;
		protected const string BadIdentifier = "I'm identifier!";

		public BaseTestController(ApplicationContextsDataFixture fixture)
		{
			this._fixture = fixture;
		}
	}
}