using Microsoft.AspNetCore.Mvc;
using SomeApiProject.Controllers;
using SomeApiProject.Data;
using SomeApiProject.Models;
using SomeApiProject.Tests.Infrastructure;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace SomeApiProject.Tests
{
	public class AccountControllerTests : BaseTestController
	{
		private AccountController _accountController { get; set; }

		public AccountControllerTests(ApplicationContextsDataFixture fixture) : base(fixture)
		{
			_accountController = new AccountController(_fixture.SomeApiContext);
		}

		[Fact]
		public void GetList_IfRequestEmpty_Fail()
		{
			var result = _accountController.GetListByCustomerId(null);
			Assert.IsType<ActionResult<List<AccountViewModel>>>(result);
			Assert.True(result.Result is BadRequestObjectResult);
		}

		[Fact]
		public void GetList_IfCutomerNotExists_Fail()
		{
			var result = _accountController.GetListByCustomerId(BadIdentifier);
			Assert.IsType<ActionResult<List<AccountViewModel>>>(result);
			Assert.True(result.Result is BadRequestObjectResult);
		}

		[Fact]
		public void GetList_IfCutomerExists_Success()
		{
			var result = _accountController.GetListByCustomerId(SomeApiDbInitializer.Customer1Id);
			Assert.IsType<ActionResult<List<AccountViewModel>>>(result);
			var count = ((List<AccountViewModel>)((OkObjectResult)result.Result).Value).Count;
			Assert.True(count > 0);
		}

		[Fact]
		public void GetById_IfRequestEmpty_Fail()
		{
			var result = _accountController.GetById(null);
			Assert.IsType<ActionResult<AccountViewModel>>(result);
			Assert.True(result.Result is BadRequestObjectResult);
		}

		[Fact]
		public void GetById_IfAccountNotExists_Fail()
		{
			var result = _accountController.GetById(BadIdentifier);
			Assert.IsType<ActionResult<AccountViewModel>>(result);
			Assert.True(result.Result is BadRequestObjectResult);
		}

		[Fact]
		public void GetById_IfAccountExists_Success()
		{
			var result = _accountController.GetById(SomeApiDbInitializer.Account1_1Id);
			Assert.IsType<ActionResult<AccountViewModel>>(result);
			var account = ((AccountViewModel)((OkObjectResult)result.Result).Value);
			Assert.True(account.Id == SomeApiDbInitializer.Account1_1Id);
		}

		[Fact]
		public void OpenNew_IfRequestEmpty_Fail()
		{
			var result = _accountController.OpenNew(null);
			Assert.IsType<ActionResult<string>>(result);
			Assert.True(result.Result is BadRequestObjectResult);
		}

		[Fact]
		public void OpenNew_IfCustomerIdEmpty_Fail()
		{
			var result = _accountController.OpenNew(new AddAccountViewModel());
			Assert.IsType<ActionResult<string>>(result);
			Assert.True(result.Result is BadRequestObjectResult);
		}

		[Fact]
		public void OpenNew_IfCustomerNotExists_Fail()
		{
			var request = new AddAccountViewModel
			{ 
				CustomerId = BadIdentifier
			};

			var result = _accountController.OpenNew(request);
			Assert.IsType<ActionResult<string>>(result);
			Assert.True(result.Result is BadRequestObjectResult);
		}

		[Fact]
		public void OpenNew_IfCustomerExistsAndCreditZero_Success()
		{
			var request = new AddAccountViewModel
			{
				CustomerId = SomeApiDbInitializer.Customer1Id
			};

			var result = _accountController.OpenNew(request);
			Assert.IsType<ActionResult<string>>(result);

			var accountId = ((string)((OkObjectResult)result.Result).Value);
			var account = _fixture.SomeApiContext.Accounts.FirstOrDefault(a => a.Id == accountId);
			Assert.True(account != null);
		}

		[Fact]
		public void OpenNew_IfCustomerExistsAndCreditNonZero_Success()
		{
			var request = new AddAccountViewModel
			{
				CustomerId = SomeApiDbInitializer.Customer1Id,
				InitialCredit = 100
			};

			var result = _accountController.OpenNew(request);
			Assert.IsType<ActionResult<string>>(result);

			var accountId = ((string)((OkObjectResult)result.Result).Value);
			var account = _fixture.SomeApiContext.Accounts.FirstOrDefault(a => a.Id == accountId);
			Assert.True(account != null);

			var transaction = _fixture.SomeApiContext.Transactions.FirstOrDefault(a => a.AccountId == accountId);
			Assert.True(transaction != null);
		}
	}
}
