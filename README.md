# Some API Project

Test project to check if I can do code on C#, set CI-CD on gitlab and show it to the world.


# How to run the project

0. Install VS - [Visual Studio Community Edition 2019](https://visualstudio.microsoft.com/vs/)
1. Clone the repo on your machine
2. In project folder run SomeApiProject.sln
3. After you have seen 'Project loaded and ready to use' on popup in the left lower corner of VS - press F5
4. Start page of the project is [Swagger interface](https://swagger.io/) - feel free to use all API-methods


# How it works

It's a .net core web API project which uses In-Memory DataBase.
So every time you start the project it creates several data records inside of your RAM - no data on 'hard drive'.
You can add records afterwards and do whatever you want till you shut down the app.
Once you have turned it off - all your data are lost.
The project is designed only for test and code review purpose.
But according to MIT license you may freely use it as you wish.

# Is there any user interface?

Absolutely!
Here → [Some UI Project](https://gitlab.com/ivan.v.konstantinov/some-ui-project)
It's a small React app, check it!